package com.bfzs.weixin.accesstoken.api;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bfzs.weixin.accesstoken.manager.AbstractTokenManager;
import com.bfzs.weixin.accesstoken.manager.AccessTokenManager;

@RestController
@RequestMapping("token")
public class AccessTokenApi extends AbstractTokenController{
	
	@Resource
	AccessTokenManager manager;

	@Override
	AbstractTokenManager getManager() {
		return manager;
	}
	
}
