package com.bfzs.weixin.accesstoken.api;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bfzs.weixin.accesstoken.manager.AbstractTokenManager;
import com.bfzs.weixin.accesstoken.manager.JSAccessTokenManager;

@RestController
@RequestMapping("jstoken")
public class JsAccessTokenApi extends AbstractTokenController{
	
	@Resource
	JSAccessTokenManager manager;

	@Override
	AbstractTokenManager getManager() {
		return manager;
	}
	
 
}
