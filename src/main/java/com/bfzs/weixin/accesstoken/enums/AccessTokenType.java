package com.bfzs.weixin.accesstoken.enums;

public enum AccessTokenType {
	WECHAT_ACCESS_TOKEN,
	JS_ACCESS_TOKEN
}
